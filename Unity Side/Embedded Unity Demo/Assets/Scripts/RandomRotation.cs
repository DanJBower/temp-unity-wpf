﻿using UnityEngine;

namespace EmbeddedUnityDemo
{
    public class RandomRotation : MonoBehaviour
    {
        [SerializeField]
        [Range(0, 100)]
        private float rotationSpeedX = 1;

        [SerializeField]
        [Range(0, 100)]
        private float rotationSpeedY = 1;

        [SerializeField]
        [Range(0, 100)]
        private float rotationSpeedZ = 1;

        [SerializeField]
        [Range(0, 360)]
        private float xAngle;

        [SerializeField]
        [Range(0, 360)]
        private float yAngle;

        [SerializeField]
        [Range(0, 360)]
        private float zAngle;

        [SerializeField]
        private bool canXRotate = true;

        [SerializeField]
        private bool canYRotate = true;

        [SerializeField]
        private bool canZRotate = true;

        private void LateUpdate()
        {
            if (canXRotate)
            {
                xAngle += rotationSpeedX;

                if (xAngle > 360)
                {
                    xAngle -= 360;
                }
            }

            if (canYRotate)
            {
                yAngle += rotationSpeedY;

                if (yAngle > 360)
                {
                    yAngle -= 360;
                }
            }

            if (canZRotate)
            {
                zAngle += rotationSpeedZ;

                if (zAngle > 360)
                {
                    zAngle -= 360;
                }
            }

            transform.localEulerAngles = new Vector3(xAngle, yAngle, zAngle);
        }
    }
}
