﻿using System;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.ComponentModel;

namespace WPFSide
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("User32.dll")]
        static extern bool MoveWindow(IntPtr handle, int x, int y, int width, int height, bool redraw);

        internal delegate int WindowEnumProc(IntPtr hwnd, IntPtr lparam);
        [DllImport("user32.dll")]
        internal static extern bool EnumChildWindows(IntPtr hwnd, WindowEnumProc func, IntPtr lParam);

        [DllImport("user32.dll")]
        static extern int SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

        private Process unityProcess;
        private IntPtr unityHWND = IntPtr.Zero;

        private const int WM_ACTIVATE = 0x0006;
        private readonly IntPtr WA_ACTIVE = new IntPtr(1);
        private readonly IntPtr WA_INACTIVE = new IntPtr(0);

        private bool isFirstTimeActivating = true;
        private bool isFirstTimeDeactivating = true;
        private bool unityProcessRunning = false;
        private const string PATH = @"..\..\..\..\..\Unity Side\Embedded Unity Demo\Build\Embedded Unity Demo.exe";

        public MainWindow()
        {
            InitializeComponent();
            SetUpUnityWindow();
        }

        private void SetUpUnityWindow()
        {
            bool exeExists = File.Exists(PATH);
            Console.WriteLine($"{Path.GetFullPath(PATH)} {(exeExists ? "does exist" : "doesn't exist")}"); //As PATH is relative, this helps make sure it is correct

            if (!exeExists)
            {
                //Unity .exe missing
                return;
            }

            IntPtr handle = UnityPanel.Handle;

            unityProcess = new Process();
            unityProcess.StartInfo.FileName = PATH;
            unityProcess.StartInfo.Arguments = "-parentHWND " + handle.ToInt32() + " " + Environment.CommandLine;
            unityProcess.StartInfo.UseShellExecute = true;
            unityProcess.StartInfo.CreateNoWindow = true;

            try
            {
                unityProcess.Start();
            }
            catch (InvalidOperationException)
            {
                //Something went wrong setting up the unity process
                return;
            }

            unityProcess.WaitForInputIdle();

            unityProcessRunning = true;

            EnumChildWindows(handle, WindowEnum, IntPtr.Zero);

            unityHWNDLabel.Content = "Unity HWND: 0x" + unityHWND.ToString("X8");
        }

        private void ActivateUnityWindow()
        {
            SendMessage(unityHWND, WM_ACTIVATE, WA_ACTIVE, IntPtr.Zero);
        }

        private void DeactivateUnityWindow()
        {
            SendMessage(unityHWND, WM_ACTIVATE, WA_INACTIVE, IntPtr.Zero);
        }

        private int WindowEnum(IntPtr hwnd, IntPtr lparam)
        {
            unityHWND = hwnd;
            ActivateUnityWindow();
            return 0;
        }

        private void OnUnityPanelResize(object sender, EventArgs e)
        {
            MoveWindow(unityHWND, 0, 0, UnityPanel.Width, UnityPanel.Height, true);
            ActivateUnityWindow();
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            if (!unityProcessRunning)
            {
                //Unity process didn't start. Nothing to close
                return;
            }

            try
            {
                unityProcess.CloseMainWindow();
            }
            catch (InvalidOperationException)
            {
                //Unity process already dead
                return;
            }
            
            while (!unityProcess.HasExited)
            {
                unityProcess.Kill();
            }
        }

        private void Activate(object sender, EventArgs e)
        {
            if (isFirstTimeActivating)
            {
                isFirstTimeActivating = false;
                ActivateUnityWindow();
            }
        }

        private void Deactivate(object sender, EventArgs e)
        {
            if (isFirstTimeDeactivating)
            {
                isFirstTimeDeactivating = false;
                DeactivateUnityWindow();
            }
        }
    }
}
